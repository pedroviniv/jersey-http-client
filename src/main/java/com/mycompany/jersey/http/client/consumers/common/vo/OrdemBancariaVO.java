/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class OrdemBancariaVO implements Serializable {

    @NotBlank(message = "{NotBlank}")
    private String numero;
    @NotBlank(message = "{NotBlank}")
    private String data;

    public OrdemBancariaVO(String numero, String data) {
        this.numero = numero;
        this.data = data;
    }

    public OrdemBancariaVO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "OrdemBancariaVO{" + "numero=" + numero + ", data=" + data + '}';
    }
}
