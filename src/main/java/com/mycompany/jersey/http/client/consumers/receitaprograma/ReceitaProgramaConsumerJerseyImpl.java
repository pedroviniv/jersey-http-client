/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.receitaprograma;

import com.mycompany.jersey.http.client.consumers.common.client.FinanceiroWebService;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PutRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import com.mycompany.jersey.http.client.consumers.common.vo.ReceitaProgramaVO;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Singleton
@Local(ReceitaProgramaConsumer.class)
public class ReceitaProgramaConsumerJerseyImpl implements ReceitaProgramaConsumer {

    @Inject
    private Client client;
    @Inject
    private Mapper mapper;

    public ReceitaProgramaConsumerJerseyImpl(Client client, Mapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    public ReceitaProgramaConsumerJerseyImpl() {
    }

    @Override
    public void persist(ReceitaProgramaVO receitaPrograma, String cidade) throws ReceitaProgramaException {

        String json = this.mapper.toString(receitaPrograma);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response postResponse = this.client.target(domain)
                .path(FinanceiroWebService.RECEITA_PROGRAMA)
                .request()
                .post(Entity.json(json));

        if (postResponse.getStatus() == 400) {

            String jsonData = postResponse.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PostRequestException(fieldErrors,
                    "Não foi possível persistir a receita de programa: " + receitaPrograma);
        } else if (postResponse.getStatus() != 201) {
            throw new ReceitaProgramaException("Coudln't persist receitaPrograma: "
                    + receitaPrograma);
        }
    }

    @Override
    public List<ReceitaProgramaVO> listAll(String cidade) throws ReceitaProgramaException {

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response getResponse = this.client.target(domain)
                .path(FinanceiroWebService.RECEITA_PROGRAMA)
                .request()
                .get();

        String jsonData = getResponse.readEntity(String.class);
        return this.mapper.toList(jsonData, ReceitaProgramaVO.class);
    }

    @Override
    public void update(ReceitaProgramaVO updatedReceitaPrograma, String cidade) {

        String convenioJson = this.mapper.toString(updatedReceitaPrograma);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response response = this.client.target(domain)
                .path(FinanceiroWebService.RECEITA_PROGRAMA)
                .request()
                .put(Entity.json(convenioJson));

        if (response.getStatus() == 400) {

            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PutRequestException(fieldErrors,
                    "Não foi possível atualizar a receita programa: "
                    + updatedReceitaPrograma + ", causa: " + fieldErrors);
        } else if (response.getStatus() != 200) {
            throw new PutRequestException("Não foi possível atualizar a receita de programa: " + updatedReceitaPrograma);
        }
    }

}
