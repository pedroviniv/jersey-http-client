/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo.enums;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public enum OrigemDespesa {

    CONVENIO("Convênio"),
    PROGRAMA("Programa");

    private final String descricao;

    OrigemDespesa(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return this.descricao;
    }
}
