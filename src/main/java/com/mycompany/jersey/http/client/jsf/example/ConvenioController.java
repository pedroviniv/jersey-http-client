/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.jsf.example;

import com.mycompany.jersey.http.client.consumers.common.vo.ConvenioVO;
import com.mycompany.jersey.http.client.consumers.convenio.ConvenioConsumer;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@ManagedBean(name = "convenioController")
@ViewScoped
public class ConvenioController {
    
    @EJB 
    private ConvenioConsumer convenioConsumer;
    private ConvenioVO convenioVO = new ConvenioVO();
    
    public void newConvenio() {
        
        System.out.println(convenioConsumer);
    }

    public ConvenioVO getConvenioVO() {
        return convenioVO;
    }

    public void setConvenioVO(ConvenioVO convenioVO) {
        this.convenioVO = convenioVO;
    }
}
