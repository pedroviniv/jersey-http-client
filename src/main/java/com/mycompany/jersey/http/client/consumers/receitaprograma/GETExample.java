/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.receitaprograma;

import com.mycompany.jersey.http.client.consumers.common.client.NonSSLJerseyClientFactory;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class GETExample {

    public static void main(String[] args) {
        
        Client client = new NonSSLJerseyClientFactory()
                .getInstance();
        
        Mapper mapper = new Mapper();
        
        ReceitaProgramaConsumer consumer = new ReceitaProgramaConsumerJerseyImpl(client, mapper);
        
        try {
            consumer.listAll("izaquiel")
                    .forEach(System.out::println);
        } catch (ReceitaProgramaException ex) {
            Logger.getLogger(GETExample.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
