/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import com.mycompany.jersey.http.client.consumers.common.vo.enums.OrigemRecurso;
import com.mycompany.jersey.http.client.consumers.common.vo.enums.TipoEntrada;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class ReceitaProgramaVO implements Serializable {

    private Long id;

    @NotBlank(message = "{NotBlank}")
    @Size(min = 4, max = 4, message = "{Size}")
    private String ano;

    @NotNull(message = "{NotNull}")
    private OrigemRecurso origemRecurso;

    private String outraOrigem;

    @NotBlank(message = "{NotBlank}")
    private String resolucao;

    @NotNull(message = "{NotNull}")
    @Valid
    private DadosBancariosVO dadosBancarios;
    @NotBlank(message = "{NotBlank}")
    private String dataCredito;
    @NotNull(message = "{NotNull}")
    private ProgramaVO programa;
    @NotNull(message = "{NotNull}")
    private BigDecimal valor;
    @NotNull(message = "{NotNull}")
    private TipoEntrada tipoEntrada;

    public ReceitaProgramaVO(Long id, String ano, OrigemRecurso origemRecurso, String outraOrigem, String resolucao, DadosBancariosVO dadosBancarios, String dataCredito, ProgramaVO programa, BigDecimal valor, TipoEntrada tipoEntrada) {
        this.id = id;
        this.ano = ano;
        this.origemRecurso = origemRecurso;
        this.outraOrigem = outraOrigem;
        this.resolucao = resolucao;
        this.dadosBancarios = dadosBancarios;
        this.dataCredito = dataCredito;
        this.programa = programa;
        this.valor = valor;
        this.tipoEntrada = tipoEntrada;
    }

    public ReceitaProgramaVO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAno() {
        return ano;
    }

    public void setAno(String ano) {
        this.ano = ano;
    }

    public OrigemRecurso getOrigemRecurso() {
        return origemRecurso;
    }

    public void setOrigemRecurso(OrigemRecurso origemRecurso) {
        this.origemRecurso = origemRecurso;
    }

    public String getOutraOrigem() {
        return outraOrigem;
    }

    public void setOutraOrigem(String outraOrigem) {
        this.outraOrigem = outraOrigem;
    }

    public String getResolucao() {
        return resolucao;
    }

    public void setResolucao(String resolucao) {
        this.resolucao = resolucao;
    }

    public DadosBancariosVO getDadosBancarios() {
        return dadosBancarios;
    }

    public void setDadosBancarios(DadosBancariosVO dadosBancarios) {
        this.dadosBancarios = dadosBancarios;
    }

    public String getDataCredito() {
        return dataCredito;
    }

    public void setDataCredito(String dataCredito) {
        this.dataCredito = dataCredito;
    }

    public ProgramaVO getPrograma() {
        return programa;
    }

    public void setPrograma(ProgramaVO programa) {
        this.programa = programa;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public TipoEntrada getTipoEntrada() {
        return tipoEntrada;
    }

    public void setTipoEntrada(TipoEntrada tipoEntrada) {
        this.tipoEntrada = tipoEntrada;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 23 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ReceitaProgramaVO other = (ReceitaProgramaVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ReceitaProgramaVO{" + "id=" + id + ", ano=" + ano + ", origemRecurso=" + origemRecurso + ", outraOrigem=" + outraOrigem + ", resolucao=" + resolucao + ", dadosBancarios=" + dadosBancarios + ", dataCredito=" + dataCredito + ", programa=" + programa + ", valor=" + valor + ", tipoEntrada=" + tipoEntrada + '}';
    }
}
