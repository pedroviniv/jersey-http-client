/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class NotaFiscalVO implements Serializable {

    @NotBlank(message = "{NotBlank}")
    private String numero;
    @NotBlank(message = "{NotBlank}")
    private String dataEmissao;

    public NotaFiscalVO(String numero, String dataEmissao) {
        this.numero = numero;
        this.dataEmissao = dataEmissao;
    }

    public NotaFiscalVO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    @Override
    public String toString() {
        return "notaFiscal{" + "numero=" + numero + ", dataEmissao=" + dataEmissao + '}';
    }
}
