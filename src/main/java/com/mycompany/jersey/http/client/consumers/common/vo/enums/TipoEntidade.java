/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo.enums;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public enum TipoEntidade {
    
    FEDERAl("Federal"), 
    ESTADUAL("Estadual"),
    MUNICIPAL("Municipal");
    
    private final String descricao;

    private TipoEntidade(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }
}
