/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.programa;

import com.mycompany.jersey.http.client.consumers.common.vo.ProgramaVO;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface ProgramaConsumer {

    List<ProgramaVO> listAll(String cidade);
    ProgramaVO findById(Long id, String cidade);
    void save(ProgramaVO programaVO, String cidade);
}
