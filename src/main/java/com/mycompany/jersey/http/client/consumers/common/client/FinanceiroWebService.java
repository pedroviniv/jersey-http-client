/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.client;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class FinanceiroWebService {

    //melhor usar um properties
    public static final String URI_TEMPLATE = "%s/%s";
    public static final String BASE_URI = "http://192.168.2.113:8080/iEscolarFinanceiro/api/v1";
    public static final String CONVENIOS = "convenios";
    public static final String REPASSES = "repasses";
    public static final String DESPESAS = "despesas";
    public static final String PROGRAMAS = "programas";
    public static final String RECEITA_PROGRAMA = "/receitaProgramas";
}
