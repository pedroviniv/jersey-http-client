/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.repasse;

import com.mycompany.jersey.http.client.consumers.common.vo.RepasseVO;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface RepasseConsumer {

    void persist(RepasseVO repasseVO, String cidade);
    List<RepasseVO> listAll(String cidade);
    void update(RepasseVO updatedRepasse, String cidade);
}
