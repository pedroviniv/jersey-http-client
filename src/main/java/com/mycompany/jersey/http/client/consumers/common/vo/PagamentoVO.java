/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo;

import com.mycompany.jersey.http.client.consumers.common.vo.enums.FormaPagamento;
import java.io.Serializable;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@XmlRootElement
public class PagamentoVO implements Serializable {
    
    
    @NotNull(message = "{NotNull}")
    private FormaPagamento formaPagamento;
    
//    @Valid
//    @NotNull(message = "{NotNull}")
    private OrdemBancariaVO ordemBancaria;
    
//    @Valid
//    @NotNull(message = "{NotNull}")
    private ChequeVO cheque;

    public PagamentoVO(FormaPagamento formaPagamento, OrdemBancariaVO ordemBancaria, ChequeVO cheque) {
        this.formaPagamento = formaPagamento;
        this.ordemBancaria = ordemBancaria;
        this.cheque = cheque;
    }
    
    public PagamentoVO() {
    }

    public FormaPagamento getFormaPagamento() {
        return formaPagamento;
    }

    public void setFormaPagamento(FormaPagamento formaPagamento) {
        this.formaPagamento = formaPagamento;
    }

    public OrdemBancariaVO getOrdemBancaria() {
        return ordemBancaria;
    }

    public void setOrdemBancaria(OrdemBancariaVO ordemBancaria) {
        this.ordemBancaria = ordemBancaria;
    }

    public ChequeVO getCheque() {
        return cheque;
    }

    public void setCheque(ChequeVO cheque) {
        this.cheque = cheque;
    }

    @Override
    public String toString() {
        return "PagamentoVO{" + "formaPagamento=" + formaPagamento + ", ordemBancaria=" + ordemBancaria + ", cheque=" + cheque + '}';
    }
}
