/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo.enums;

/**
 *
 * @author Wensttay de Sousa Alencar <yattsnew@gmail.com>
 */
public enum TipoDestino {
    ESCOLA("Escola"),
    SECRETARIA("Secretaria");
    
    private String nome;

    private TipoDestino(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }
}
