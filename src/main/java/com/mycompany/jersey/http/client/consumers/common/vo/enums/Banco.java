/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo.enums;

/**
 *
 * @author Wensttay de Sousa Alencar <yattsnew@gmail.com>
 */
public enum Banco {
    BANCO_DO_BRASIL("Banco do Brasil"),
    BANCO_DO_NORDESTE("Banco do Nordeste"),
    BRADESCO("Bradesco"),
    CAIXA("Caixa"),
    SANTANDER("Santander"),
    ITAU("Itau");

    private final String nome;

    Banco(String nome) {
        this.nome = nome;
    }

    public String getNome() {
        return nome;
    }

}
