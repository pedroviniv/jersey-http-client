/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.convenio;

import com.mycompany.jersey.http.client.consumers.common.vo.ConvenioVO;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface ConvenioConsumer {

    void persist(ConvenioVO convenioVO, String cidade);
    List<ConvenioVO> listAll(String cidade);
    void update(ConvenioVO updatedConvenio, String cidade);
}
