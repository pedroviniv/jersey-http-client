/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo;

import com.mycompany.jersey.http.client.consumers.common.vo.enums.Banco;
import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@XmlRootElement
public class DadosBancariosVO implements Serializable {

    @NotNull(message = "{NotNull}")
    private Banco banco;
    @NotBlank(message = "{NotNull}")
    private String agencia;
    @NotBlank(message = "{NotNull}")
    private String operacao;
    private String numeroConta;

    public DadosBancariosVO(Banco banco, String agencia, String operacao, String numeroConta) {
        this.banco = banco;
        this.agencia = agencia;
        this.operacao = operacao;
        this.numeroConta = numeroConta;
    }

    public DadosBancariosVO() {
    }

    public Banco getBanco() {
        return banco;
    }

    public void setBanco(Banco banco) {
        this.banco = banco;
    }

    public String getAgencia() {
        return agencia;
    }

    public void setAgencia(String agencia) {
        this.agencia = agencia;
    }

    public String getOperacao() {
        return operacao;
    }

    public void setOperacao(String operacao) {
        this.operacao = operacao;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    @Override
    public String toString() {
        return "DadosBancariosVO{" + "banco=" + banco + ", agencia=" + agencia + ", operacao=" + operacao + ", numeroConta=" + numeroConta + '}';
    }
}
