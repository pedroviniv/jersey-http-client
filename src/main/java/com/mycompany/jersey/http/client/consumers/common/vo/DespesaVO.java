/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import com.mycompany.jersey.http.client.consumers.common.vo.enums.OrigemDespesa;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class DespesaVO implements Serializable {

    private Long id;

    @Valid
    @NotNull(message = "{NotNull}")
    private NotaFiscalVO notaFiscal;

    @NotNull(message = "{NotNull}")
    private BigDecimal valor;

    @Valid
    @NotNull(message = "{NotNull}")
    private EmpenhoVO empenho;

    @Valid
    @NotNull(message = "{NotNull}")
    private PagamentoVO pagamento;

    @NotNull(message = "{NotNull}")
    private OrigemDespesa origemDespesa;
    @NotNull(message = "{NotNull}")
    private ProgramaVO programa;
    @NotNull(message = "{NotNull}")
    private ConvenioVO convenio;
    @NotNull(message = "{NotNull}")
    private FornecedorVO fornecedor;
    @NotNull(message = "{NotNull}")
    private LicitacaoVO licitacao;

    public DespesaVO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public NotaFiscalVO getNotaFiscal() {
        return notaFiscal;
    }

    public void setNotaFiscal(NotaFiscalVO notaFiscal) {
        this.notaFiscal = notaFiscal;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public EmpenhoVO getEmpenho() {
        return empenho;
    }

    public void setEmpenho(EmpenhoVO empenho) {
        this.empenho = empenho;
    }

    public PagamentoVO getPagamento() {
        return pagamento;
    }

    public void setPagamento(PagamentoVO pagamento) {
        this.pagamento = pagamento;
    }

    public OrigemDespesa getOrigemDespesa() {
        return origemDespesa;
    }

    public void setOrigemDespesa(OrigemDespesa origemDespesa) {
        this.origemDespesa = origemDespesa;
    }

    public ProgramaVO getPrograma() {
        return programa;
    }

    public void setPrograma(ProgramaVO programa) {
        this.programa = programa;
    }

    public ConvenioVO getConvenio() {
        return convenio;
    }

    public void setConvenio(ConvenioVO convenio) {
        this.convenio = convenio;
    }

    public FornecedorVO getFornecedor() {
        return fornecedor;
    }

    public void setFornecedor(FornecedorVO fornecedor) {
        this.fornecedor = fornecedor;
    }

    public LicitacaoVO getLicitacao() {
        return licitacao;
    }

    public void setLicitacao(LicitacaoVO licitacao) {
        this.licitacao = licitacao;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 73 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DespesaVO other = (DespesaVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DespesaVO{" + "id=" + id + ", notaFiscal=" + notaFiscal + ", valor=" + valor + ", empenho=" + empenho + ", pagamento=" + pagamento + ", origemDespesa=" + origemDespesa + ", programa=" + programa + ", convenio=" + convenio + ", fornecedor=" + fornecedor + ", licitacao=" + licitacao + '}';
    }
}
