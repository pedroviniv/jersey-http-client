/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.repasse;

import com.mycompany.jersey.http.client.consumers.common.client.FinanceiroWebService;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PutRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import com.mycompany.jersey.http.client.consumers.common.vo.RepasseVO;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Singleton
@Local(RepasseConsumer.class)
public class RepasseConsumerJerseyImpl implements RepasseConsumer {

    @Inject
    private Client client;
    @Inject
    private Mapper mapper;

    public RepasseConsumerJerseyImpl(Client client, Mapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    public RepasseConsumerJerseyImpl() {
    }

    @Override
    public void persist(RepasseVO repasseVO, String cidade) {

        String json = this.mapper.toString(repasseVO);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response postResponse = this.client.target(domain)
                .path(FinanceiroWebService.REPASSES)
                .request()
                .post(Entity.json(json));

        if (postResponse.getStatus() == 400) {

            String jsonData = postResponse.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PostRequestException(fieldErrors,
                    "Não foi possível persistir a receita de programa: "
                    + repasseVO);
        }
    }

    @Override
    public List<RepasseVO> listAll(String cidade) {

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response getResponse = this.client.target(domain)
                .path(FinanceiroWebService.REPASSES)
                .request()
                .get();

        String jsonData = getResponse.readEntity(String.class);
        return this.mapper.toList(jsonData, RepasseVO.class);
    }

    @Override
    public void update(RepasseVO updatedRepasse, String cidade) {
        String convenioJson = this.mapper.toString(updatedRepasse);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response response = this.client.target(domain)
                .path(FinanceiroWebService.REPASSES)
                .request()
                .put(Entity.json(convenioJson));

        if (response.getStatus() == 400) {

            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PutRequestException(fieldErrors,
                    "Não foi possível atualizar o repasse: "
                    + updatedRepasse + ", causa: " + fieldErrors);
        } else if (response.getStatus() != 200) {
            throw new PutRequestException("Não foi possível atualizar o repasse: " + updatedRepasse);
        }
    }
}
