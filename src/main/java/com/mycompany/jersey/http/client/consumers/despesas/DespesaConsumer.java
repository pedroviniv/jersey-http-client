/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.despesas;

import com.mycompany.jersey.http.client.consumers.common.vo.DespesaVO;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface DespesaConsumer {

    void persist(DespesaVO despesaVO, String cidade);
    List<DespesaVO> listAll(String cidade);
    void udpate(DespesaVO updatedDespesa, String cidade);
}
