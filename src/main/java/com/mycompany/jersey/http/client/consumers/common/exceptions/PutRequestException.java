/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.exceptions;

import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class PutRequestException extends BadRequestException {
    
    private List<FieldError> fieldErrors;

    public PutRequestException(List<FieldError> fieldErrors, String message) {
        super(message);
        this.fieldErrors = fieldErrors;
    }

    public PutRequestException(List<FieldError> fieldErrors, Response response) {
        super(response);
        this.fieldErrors = fieldErrors;
    }

    public PutRequestException(List<FieldError> fieldErrors, String message, Response response) {
        super(message, response);
        this.fieldErrors = fieldErrors;
    }

    public PutRequestException(List<FieldError> fieldErrors, Throwable cause) {
        super(cause);
        this.fieldErrors = fieldErrors;
    }

    public PutRequestException() {
    }

    public PutRequestException(String message) {
        super(message);
    }

    public PutRequestException(Response response) {
        super(response);
    }

    public PutRequestException(String message, Response response) {
        super(message, response);
    }

    public List<FieldError> getFieldErrors() {
        return fieldErrors;
    }
}
