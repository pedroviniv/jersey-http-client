/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.client;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Default;
import javax.enterprise.inject.Produces;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@ApplicationScoped
public class NonSSLJerseyClientFactory implements ClientFactory {

    @Override
    @Produces
    @ApplicationScoped
    @Default
    public Client getInstance() {
        
        return ClientBuilder.newClient();
    }
}
