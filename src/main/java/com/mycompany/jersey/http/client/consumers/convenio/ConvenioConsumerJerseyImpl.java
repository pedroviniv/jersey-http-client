/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.convenio;

import com.mycompany.jersey.http.client.consumers.common.client.FinanceiroWebService;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PutRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.ConvenioVO;
import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Singleton
@Local(ConvenioConsumer.class)
public class ConvenioConsumerJerseyImpl implements ConvenioConsumer {
    
    @Inject private Client client;
    @Inject private Mapper mapper;

    public ConvenioConsumerJerseyImpl(Client client, Mapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    public ConvenioConsumerJerseyImpl() {
    }

    @Override
    public void persist(ConvenioVO convenioVO, String cidade) {
        
        String convenioJson = this.mapper.toString(convenioVO);
        
        String domain = String.format(FinanceiroWebService.URI_TEMPLATE, 
                FinanceiroWebService.BASE_URI, cidade);
        
        Response response = this.client.target(domain)
                .path(FinanceiroWebService.CONVENIOS)
                .request()
                .post(Entity.json(convenioJson));
        
        if(response.getStatus() != 201) {
            
            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);
            
            throw new PostRequestException(fieldErrors, 
                    "Não foi possível persistir o convenio: " + convenioVO);
        }
    }

    @Override
    public List<ConvenioVO> listAll(String cidade){
        
        String domain = String.format(FinanceiroWebService.URI_TEMPLATE, 
                FinanceiroWebService.BASE_URI, cidade);
        
        Response getResponse = this.client
                .target(domain)
                .path(FinanceiroWebService.CONVENIOS)
                .request().get();
        
        String jsonResponse = getResponse.readEntity(String.class);
        
        return this.mapper.toList(jsonResponse, ConvenioVO.class);
    }

    @Override
    public void update(ConvenioVO updatedConvenio, String cidade) {
        
        String convenioJson = this.mapper.toString(updatedConvenio);
        
        String domain = String.format(FinanceiroWebService.URI_TEMPLATE, 
                FinanceiroWebService.BASE_URI, cidade);
        
        Response response = this.client.target(domain)
                .path(FinanceiroWebService.CONVENIOS)
                .request()
                .put(Entity.json(convenioJson));
        
        if(response.getStatus() == 400) {
            
            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);
            
            throw new PutRequestException(fieldErrors, 
                    "Não foi possível persistir o convenio: "
                            + ", cause: " + fieldErrors);
        }
        
        else if(response.getStatus() == 200) {
            throw new PutRequestException("Não foi possível persistir o convenio: " + updatedConvenio);
        }
    }

}
