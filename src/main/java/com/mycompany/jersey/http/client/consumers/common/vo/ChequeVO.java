/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@XmlRootElement
public class ChequeVO implements Serializable {

    @NotNull(message = "{NotNull}")
    private String numero;
    @NotNull(message = "{NotNull}")
    private String dataEmissao;
    @NotNull(message = "{NotNull}")
    private String dataCompensacao;

    public ChequeVO(String numero, String dataEmissao, String dataCompensacao) {
        this.numero = numero;
        this.dataEmissao = dataEmissao;
        this.dataCompensacao = dataCompensacao;
    }

    public ChequeVO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getDataEmissao() {
        return dataEmissao;
    }

    public void setDataEmissao(String dataEmissao) {
        this.dataEmissao = dataEmissao;
    }

    public String getDataCompensacao() {
        return dataCompensacao;
    }

    public void setDataCompensacao(String dataCompensacao) {
        this.dataCompensacao = dataCompensacao;
    }

    @Override
    public String toString() {
        return "ChequeVO{" + "numero=" + numero + ", dataEmissao=" + dataEmissao + ", dataCompensacao=" + dataCompensacao + '}';
    }
}
