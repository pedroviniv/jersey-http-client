/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import com.mycompany.jersey.http.client.consumers.common.vo.enums.TipoDestino;
import com.mycompany.jersey.http.client.consumers.common.vo.enums.TipoEntidade;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class ConvenioVO implements Serializable {

    private Long id;
    @NotNull(message = "{NotNull}")
    private TipoEntidade entidade;
    @NotNull(message = "{NotNull}")
    private TipoDestino destino;
    @NotBlank(message = "{NotBlank}")
    private String numeroConvenio;
    @NotBlank(message = "{NotBlank}")
    private String objeto;
    @Min(value = 1, message = "{Size}")
    private BigDecimal valorRecurso;
    @Min(value = 1, message = "{Size}")
    private BigDecimal valorContraPartida;
    @Pattern(regexp = "[0-2][0-9]/[0-1][0-9]/[0-9]{4}", message = "{Date}")
    @NotBlank(message = "{NotBlank}")
    private String dataInicio;
    @Pattern(regexp = "[0-2][0-9]/[0-1][0-9]/[0-9]{4}", message = "{Date}")
    @NotBlank(message = "{NotBlank}")
    private String dataFim;
    @Pattern(regexp = "[0-2][0-9]/[0-1][0-9]/[0-9]{4}", message = "{Date}")
    @NotBlank(message = "{NotBlank}")
    private String dataCelebracao;
    private boolean contraPartida;
    private boolean possuiContaBancaria;
    @NotNull(message = "{NotNull}")
    private EscolaVO escola;
    @NotNull(message = "{NotNull}")
    @Valid
    private DadosBancariosVO dadosBancarios;

    public ConvenioVO(Long id, TipoEntidade entidade, TipoDestino destino, String numeroConvenio, String objeto, BigDecimal valorRecurso, BigDecimal valorContraPartida, String dataInicio, String dataFim, String dataCelebracao, boolean contraPartida, boolean possuiContaBancaria, EscolaVO escola, DadosBancariosVO dadosBancarios) {
        this.id = id;
        this.entidade = entidade;
        this.destino = destino;
        this.numeroConvenio = numeroConvenio;
        this.objeto = objeto;
        this.valorRecurso = valorRecurso;
        this.valorContraPartida = valorContraPartida;
        this.dataInicio = dataInicio;
        this.dataFim = dataFim;
        this.dataCelebracao = dataCelebracao;
        this.contraPartida = contraPartida;
        this.possuiContaBancaria = possuiContaBancaria;
        this.escola = escola;
        this.dadosBancarios = dadosBancarios;
    }

    public ConvenioVO() {
    }

    public TipoDestino getDestino() {
        return destino;
    }

    public void setDestino(TipoDestino destino) {
        this.destino = destino;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TipoEntidade getEntidade() {
        return entidade;
    }

    public void setEntidade(TipoEntidade entidade) {
        this.entidade = entidade;
    }

    public String getNumeroConvenio() {
        return numeroConvenio;
    }

    public void setNumeroConvenio(String numeroConvenio) {
        this.numeroConvenio = numeroConvenio;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }

    public BigDecimal getValorRecurso() {
        return valorRecurso;
    }

    public void setValorRecurso(BigDecimal valorRecurso) {
        this.valorRecurso = valorRecurso;
    }

    public BigDecimal getValorContraPartida() {
        return valorContraPartida;
    }

    public void setValorContraPartida(BigDecimal valorContraPartida) {
        this.valorContraPartida = valorContraPartida;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public String getDataFim() {
        return dataFim;
    }

    public void setDataFim(String dataFim) {
        this.dataFim = dataFim;
    }

    public String getDataCelebracao() {
        return dataCelebracao;
    }

    public void setDataCelebracao(String dataCelebracao) {
        this.dataCelebracao = dataCelebracao;
    }

    public boolean isContraPartida() {
        return contraPartida;
    }

    public void setContraPartida(boolean contraPartida) {
        this.contraPartida = contraPartida;
    }

    public boolean isPossuiContaBancaria() {
        return possuiContaBancaria;
    }

    public void setPossuiContaBancaria(boolean possuiContaBancaria) {
        this.possuiContaBancaria = possuiContaBancaria;
    }

    public EscolaVO getEscola() {
        return escola;
    }

    public void setEscola(EscolaVO escola) {
        this.escola = escola;
    }

    public DadosBancariosVO getDadosBancarios() {
        return dadosBancarios;
    }

    public void setDadosBancarios(DadosBancariosVO dadosBancarios) {
        this.dadosBancarios = dadosBancarios;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 83 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ConvenioVO other = (ConvenioVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ConvenioVO{" + "id=" + id + ", entidade=" + entidade + ", destino=" + destino + ", numeroConvenio=" + numeroConvenio + ", objeto=" + objeto + ", valorRecurso=" + valorRecurso + ", valorContraPartida=" + valorContraPartida + ", dataInicio=" + dataInicio + ", dataFim=" + dataFim + ", dataCelebracao=" + dataCelebracao + ", contraPartida=" + contraPartida + ", possuiContaBancaria=" + possuiContaBancaria + ", escola=" + escola + ", dadosBancarios=" + dadosBancarios + '}';
    }
}
