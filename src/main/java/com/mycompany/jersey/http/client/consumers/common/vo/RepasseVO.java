/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class RepasseVO implements Serializable {

    private Long id;
    @NotNull(message = "{NotNull}")
    private ProgramaVO programa;
    @NotNull(message = "{NotNull}")
    private List<ItemRepasseVO> itensRepasse;

    public RepasseVO(Long id, ProgramaVO programa, List<ItemRepasseVO> itensRepasse) {
        this.id = id;
        this.programa = programa;
        this.itensRepasse = itensRepasse;
    }

    public RepasseVO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProgramaVO getPrograma() {
        return programa;
    }

    public void setPrograma(ProgramaVO programa) {
        this.programa = programa;
    }

    public List<ItemRepasseVO> getItensRepasse() {
        return itensRepasse;
    }

    public void setItensRepasse(List<ItemRepasseVO> itensRepasse) {
        this.itensRepasse = itensRepasse;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final RepasseVO other = (RepasseVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "RepasseVO{" + "id=" + id + ", programa=" + programa + ", itensRepasse=" + itensRepasse + '}';
    }
}
