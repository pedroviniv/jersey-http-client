/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.convenio;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class ConvenioConsumerException extends Exception {

    public ConvenioConsumerException(String message) {
        super(message);
    }

    public ConvenioConsumerException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConvenioConsumerException(Throwable cause) {
        super(cause);
    }

    public ConvenioConsumerException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
