/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class FieldError implements Serializable {

    private List<String> errorType;
    private String message;

    public FieldError(List<String> errorType, String message) {
        this.errorType = errorType;
        this.message = message;
    }

    public FieldError() {
    }

    public List<String> getErrorType() {
        return errorType;
    }

    public void setErrorType(List<String> errorType) {
        this.errorType = errorType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return "Error{" + "errorType=" + errorType + ", message=" + message + '}';
    }
}
