/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.despesas;

import com.mycompany.jersey.http.client.consumers.common.client.FinanceiroWebService;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PutRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.DespesaVO;
import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@Singleton
@Local(DespesaConsumer.class)
public class DespesaConsumerJerseyImpl implements DespesaConsumer {

    @Inject
    Mapper mapper;
    @Inject
    Client client;

    public DespesaConsumerJerseyImpl(Mapper mapper, Client client) {
        this.mapper = mapper;
        this.client = client;
    }

    public DespesaConsumerJerseyImpl() {
    }

    @Override
    public void persist(DespesaVO despesaVO, String cidade) {

        String convenioJson = this.mapper.toString(despesaVO);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response response = this.client.target(domain)
                .path(FinanceiroWebService.DESPESAS)
                .request()
                .post(Entity.json(convenioJson));

        if (response.getStatus() == 400) {

            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PostRequestException(fieldErrors,
                    "Não foi possível persistir a despesa: " + despesaVO);
        }
    }

    @Override
    public List<DespesaVO> listAll(String cidade) {

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response getResponse = this.client
                .target(domain)
                .path(FinanceiroWebService.DESPESAS)
                .request().get();

        String jsonResponse = getResponse.readEntity(String.class);

        return this.mapper.toList(jsonResponse, DespesaVO.class);
    }

    @Override
    public void udpate(DespesaVO updatedDespesa, String cidade) {
        
        String convenioJson = this.mapper.toString(updatedDespesa);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response response = this.client.target(domain)
                .path(FinanceiroWebService.DESPESAS)
                .request()
                .put(Entity.json(convenioJson));

        if (response.getStatus() == 400) {

            String jsonData = response.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PutRequestException(fieldErrors,
                    "Não foi possível atualizar a despesa: "
                            + updatedDespesa + ", causa: " + fieldErrors);
        }
        else if(response.getStatus() != 200) {
            throw new PutRequestException("Não foi possível atualizar a despesa: " + updatedDespesa);
        }
    }

}
