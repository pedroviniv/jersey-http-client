/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.despesas;

import com.mycompany.jersey.http.client.consumers.common.client.NonSSLJerseyClientFactory;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import javax.ws.rs.client.Client;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class GETExample {

    public static void main(String[] args) {
        
        Client client = new NonSSLJerseyClientFactory()
                .getInstance();
        
        Mapper mapper = new Mapper();
        
        DespesaConsumer consumer = new DespesaConsumerJerseyImpl(mapper, client);
        
        consumer.listAll("izaquiel")
                .forEach(System.out::println);
    }
}
