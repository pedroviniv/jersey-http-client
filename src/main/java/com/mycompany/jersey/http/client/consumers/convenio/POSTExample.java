/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.convenio;

import com.mycompany.jersey.http.client.consumers.common.client.ClientFactory;
import com.mycompany.jersey.http.client.consumers.common.client.NonSSLJerseyClientFactory;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.ConvenioVO;
import com.mycompany.jersey.http.client.consumers.common.vo.DadosBancariosVO;
import com.mycompany.jersey.http.client.consumers.common.vo.EscolaVO;
import com.mycompany.jersey.http.client.consumers.common.vo.enums.Banco;
import com.mycompany.jersey.http.client.consumers.common.vo.enums.TipoDestino;
import com.mycompany.jersey.http.client.consumers.common.vo.enums.TipoEntidade;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class POSTExample {

    private static final Logger LOG = Logger.getLogger(POSTExample.class.getName());

    public static void main(String[] args) {
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        
        ClientFactory client = new NonSSLJerseyClientFactory();
        Mapper mapper = new Mapper();
        
        ConvenioConsumer consumer = 
                new ConvenioConsumerJerseyImpl(client.getInstance(), mapper);
        
        ConvenioVO convenioVO = new ConvenioVO();
        convenioVO.setContraPartida(true);
        
        DadosBancariosVO dadosBancarios = new DadosBancariosVO();
        dadosBancarios.setAgencia("adsasd");
        dadosBancarios.setBanco(Banco.BANCO_DO_BRASIL);
        dadosBancarios.setNumeroConta("123123123");
        dadosBancarios.setOperacao("");
        
        convenioVO.setDadosBancarios(dadosBancarios);
        convenioVO.setDataCelebracao(dtf.format(LocalDate.now()));
        convenioVO.setDataFim(dtf.format(LocalDate.now()));
        convenioVO.setDataInicio(dtf.format(LocalDate.now()));
        convenioVO.setEntidade(TipoEntidade.ESTADUAL);
        convenioVO.setEscola(new EscolaVO(12L, null));
        convenioVO.setDestino(TipoDestino.ESCOLA);
        convenioVO.setNumeroConvenio("28/2017");
        convenioVO.setObjeto("Algum Objeto");
        convenioVO.setPossuiContaBancaria(true);
        convenioVO.setValorContraPartida(BigDecimal.valueOf(10000d));
        convenioVO.setValorRecurso(BigDecimal.valueOf(1000000d));
        
        try {
            consumer.persist(convenioVO, "izaquiel");
        } catch (PostRequestException ex) {
            LOG.log(Level.SEVERE, "", ex);
        }
    }
}
