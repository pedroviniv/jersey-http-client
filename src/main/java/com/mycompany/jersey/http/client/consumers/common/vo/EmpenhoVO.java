/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.validator.constraints.NotBlank;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
@XmlRootElement
public class EmpenhoVO implements Serializable {

    @NotBlank(message = "{NotBlank}")
    private String numero;

    public EmpenhoVO(String numero) {
        this.numero = numero;
    }

    public EmpenhoVO() {
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    @Override
    public String toString() {
        return "EmpenhoVO{" + "numero=" + numero + '}';
    }
}
