/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.exceptions;

import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import java.util.List;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class PostRequestException extends BadRequestException {
    
    private List<FieldError> fieldErrors;

    public PostRequestException(List<FieldError> fieldErrors, String message) {
        super(message);
        this.fieldErrors = fieldErrors;
    }

    public PostRequestException(List<FieldError> fieldErrors, Response response) {
        super(response);
        this.fieldErrors = fieldErrors;
    }

    public PostRequestException(List<FieldError> fieldErrors, String message, Response response) {
        super(message, response);
        this.fieldErrors = fieldErrors;
    }

    public PostRequestException(List<FieldError> fieldErrors, Throwable cause) {
        super(cause);
        this.fieldErrors = fieldErrors;
    }

    public PostRequestException(String message) {
        super(message);
    }

    public PostRequestException(Response response) {
        super(response);
    }

    public PostRequestException(String message, Response response) {
        super(message, response);
    }

    public PostRequestException(Throwable cause) {
        super(cause);
    }
}
