/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.receitaprograma;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public class ReceitaProgramaException extends Exception {

    public ReceitaProgramaException(String message) {
        super(message);
    }

    public ReceitaProgramaException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReceitaProgramaException(Throwable cause) {
        super(cause);
    }
}
