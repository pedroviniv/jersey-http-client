/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.receitaprograma;

import com.mycompany.jersey.http.client.consumers.common.vo.ReceitaProgramaVO;
import java.util.List;


/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */
public interface ReceitaProgramaConsumer {

    void persist(ReceitaProgramaVO receitaPrograma, String cidade) throws ReceitaProgramaException;
    List<ReceitaProgramaVO> listAll(String cidade) throws ReceitaProgramaException;
    void update(ReceitaProgramaVO updatedReceitaPrograma, String cidade);
}
