/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.programa;

import com.mycompany.jersey.http.client.consumers.common.client.FinanceiroWebService;
import com.mycompany.jersey.http.client.consumers.common.exceptions.PostRequestException;
import com.mycompany.jersey.http.client.consumers.common.mapper.Mapper;
import com.mycompany.jersey.http.client.consumers.common.vo.FieldError;
import com.mycompany.jersey.http.client.consumers.common.vo.ProgramaVO;
import com.mycompany.jersey.http.client.consumers.receitaprograma.ReceitaProgramaException;
import java.util.List;
import javax.ejb.Local;
import javax.ejb.Singleton;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@Singleton
@Local(ProgramaConsumer.class)
public class ProgramaConsumerJerseyImpl implements ProgramaConsumer {
    
    @Inject private Client client;
    @Inject private Mapper mapper;

    public ProgramaConsumerJerseyImpl() {
    }

    public ProgramaConsumerJerseyImpl(Client client, Mapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Override
    public List<ProgramaVO> listAll(String cidade) {
        
        String domain = String.format(FinanceiroWebService.URI_TEMPLATE, 
                        FinanceiroWebService.BASE_URI, cidade);
        
        Response getResponse = this.client.target(domain)
                .path(FinanceiroWebService.PROGRAMAS)
                .request()
                .get();
        
        String json = getResponse.readEntity(String.class);
        return this.mapper.toList(json, ProgramaVO.class);
    }

    @Override
    public ProgramaVO findById(Long id, String cidade) {
        
        String domain = String.format(FinanceiroWebService.URI_TEMPLATE, 
                        FinanceiroWebService.BASE_URI, cidade);
        
        Response getResponse = this.client.target(domain)
                .path(FinanceiroWebService.PROGRAMAS)
                .path(String.valueOf(id))
                .request()
                .get();
        
        String json = getResponse.readEntity(String.class);
        
        return this.mapper.toObject(json, ProgramaVO.class);
    }

    @Override
    public void save(ProgramaVO programaVO, String cidade) {
        
        String json = this.mapper.toString(programaVO);

        String domain = String.format(FinanceiroWebService.URI_TEMPLATE,
                FinanceiroWebService.BASE_URI, cidade);

        Response postResponse = this.client.target(domain)
                .path(FinanceiroWebService.PROGRAMAS)
                .request()
                .post(Entity.json(json));

        if (postResponse.getStatus() == 400) {

            String jsonData = postResponse.readEntity(String.class);
            List<FieldError> fieldErrors = this.mapper.toList(jsonData, FieldError.class);

            throw new PostRequestException(fieldErrors,
                    "Não foi possível persistir o programa: " + programaVO);
        } else if (postResponse.getStatus() != 201) {
            throw new PostRequestException("Não foi possível persistir o programa: "
                    + programaVO);
        }
    }
}
