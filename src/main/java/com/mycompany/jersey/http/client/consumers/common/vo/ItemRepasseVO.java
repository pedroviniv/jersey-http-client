/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mycompany.jersey.http.client.consumers.common.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Pedro Arthur <pfernandesvasconcelos@gmail.com>
 */

@XmlRootElement
public class ItemRepasseVO implements Serializable {
    
    private Long id;
    @NotNull(message = "{NotNull}")
    private BigDecimal valor;
    @NotNull(message = "{NotNull}")
    private EscolaVO escola;

    public ItemRepasseVO(Long id, BigDecimal valor, EscolaVO escola) {
        this.id = id;
        this.valor = valor;
        this.escola = escola;
    }

    public ItemRepasseVO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public EscolaVO getEscola() {
        return escola;
    }

    public void setEscola(EscolaVO escola) {
        this.escola = escola;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final ItemRepasseVO other = (ItemRepasseVO) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ItemRepasseVO{" + "id=" + id + ", valor=" + valor + ", escola=" + escola + '}';
    }
}
